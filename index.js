// [SECTION] Creating Documents
/*
    Syntax: 
    
    db.<collection_name>.insertOne({
        
        "field_1": "value",
        "field_2": "value"

    })
    
    db.<collection_name>.insertMany([
        {
        
            "field_1": "value",
            "field_2": "value"

        },
        {
        
            "field_1": "value",
            "field_2": "value"

        }
    ])
*/

db.users.insertOne({

    "username": "hillaryalmonte",
    "password": "ry1234"

})

db.users.insertMany([
    {
        "username": "marcus123",
        "password": "mar1234"
    },
    {
        "username": "makoy123",
        "password": "mak1234"
    }
])

db.products.insertOne({
    "name": "Razer Blackshark V2",
    "price": 6000
})

// [SECTION] Retrieving Documents
/*
    Syntax: 

    db.<collection_name>.find() -> display all documents

    db.<collection_name>.find("field_1": "value") -> display all document that matches the criteria

    db.<collection_name>.findOne() -> display the first documents

    db.<collection_name>.findOne("field_1": "value") -> display the first document that matches the criteria  
*/
db.users.find({})

db.users.find({"username":"makoy123"})

db.cars.findOne({})

db.cars.findOne({"type": "sedan"})

// [SECTION] Updating Documents
/*
    Syntax: 

    db.<collection_name>.updateOne({},
        {
            $set: {"field_1": "new_value"}
        }
    ) -> updates the first document

    db.<collection_name>.updateOne(
        {
            "field_1": "value"
        },
        {
            $set: {"field_1": "new_value"}
        }
    ) -> updates the first document that matches the criteria

    db.<collection_name>.updateMany({},
        {
            $set: {"field_1": "new_value"}
        }
    ) -> updates all documents

     db.<collection_name>.updateMany(
        {
            "field_1": "value"
        },
        {
            $set: {"field_1": "new_value"}
        }
    ) -> updates all documents that matches the criteria

    **if the field inside $set is not existing, it will create a new one for the document 
*/

db.users.updateOne(
    {
        username: "makoy123"
    },
    {
        $set:{username: "makoy926"}
    }
)

db.users.updateOne({},
    {
        $set:{username: "dahyunieTwice"}
    }
)

db.users.updateOne(
    {
        username: "marcus123"
    },
    {
        $set:{isAdmin: true}
    }
)

db.users.updateMany({},
    {
        $set:{isAdmin: true}
    }
)

db.cars.updateMany(
    {
        type: "sedan"
    },
    {
        $set:{price: 1000000}
    }
)

// [SECTION] Delete Documents
/*
    Syntax: 
    
    db.<collection_name>.deleteOne() -> deletes the first documents

    db.<collection_name>.deleteOne("field_1": "value") -> delete the first document that matches the criteria

    db.<collection_name>.deleteMany() -> deletes all documents

    db.<collection_name>.deleteMany("field_1": "value") -> deletes all document that matches the criteria
*/
db.users.deleteOne({})

db.users.deleteOne(({type: "sedan"})

db.cars.deleteMany({})

db.users.deleteMany({isAdmin: true})